FROM tomcat

MAINTAINER Max Mustermann <max.mustermann@test.de>

COPY target/*.war /usr/local/tomcat/webapps