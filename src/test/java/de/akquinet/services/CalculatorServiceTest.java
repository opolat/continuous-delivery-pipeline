package de.akquinet.services;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorServiceTest {

	static CalculatorService calculatorService;
	
	@BeforeClass
	public static void init() {
		calculatorService = new CalculatorService();
	}
	
	@Test
	public void sum() {
		
		// Given
		int x = 5;
		int y = 6;
		
		//When
		int result = calculatorService.sum(x, y);
		
		//Then
		assertEquals(11, result);
	}
	
	@Test
	public void diff() {
		
		// Given
		int x = 10;
		int y = 4;
		
		//When
		int result = calculatorService.diff(x, y);
		
		//Then
		assertEquals(6, result);
	}
	
	@Test
	public void mult() {
		
		// Given
		int x = 10;
		int y = 4;
		
		//When
		int result = calculatorService.mult(x, y);
		
		//Then
		assertEquals(40, result);
	}
	
	@Test
	public void div() {
		
		// Given
		int x = 40;
		int y = 4;
		
		//When
		int result = calculatorService.div(x, y);
		
		//Then
		assertEquals(19, result);
	}

}
