package de.akquinet.services;

/**
 * Calculator class for demonstarting some junit methods.
 * 
 * @author opolat
 *
 */
public class CalculatorService {

	public int sum (int x, int y) {
		return x + y;
	}

	public int diff(int x, int y) {
		return x - y;
	}

	public int mult(int x, int y) {
		return x * y;
	}

	public int div(int x, int y) {
		return x/y;
	}
	
}
